import React, { useReducer } from 'react';
import { Switch, Route, Redirect } from "react-router";
import styled from 'styled-components';

import { Regions } from "./pages/Regions/Regions";
import { ContextApp, initialState, getRegionReducer } from "./reducers/getRegion";

import './app.css';

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export default function App() {
  const [state, dispatch] = useReducer(getRegionReducer, initialState);

  return (
    <ContextApp.Provider value={{dispatch, state}}>
      <Wrapper>
        <Switch>
          <Redirect
            exact
            from='/'
            to='/regions'
          />

          <Route
            path='/regions'
            component={Regions}
          />
        </Switch>
      </Wrapper>
    </ContextApp.Provider>
  );
};
