import React from 'react';
import { Switch, Route } from "react-router";

import { List } from "./containers/List/List";
import { Join } from "./containers/Join/Join";

export function Regions() {
  return (
    <Switch>
      <Route
        exact
        path='/regions'
        component={List}
      />

      <Route
        exact
        path='/regions/:id'
        component={Join}
      />
    </Switch>
  );
}
