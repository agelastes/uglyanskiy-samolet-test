import React, { useContext } from 'react';
import styled from 'styled-components';
import { Link } from "react-router-dom";

import { ContextApp } from "../../../../reducers/getRegion";
import library from '../../../../assets/icons/library.png';

const Wrapper = styled(Link)`
  width: 100%;
  display: flex;
  flex-direction: column;
  background-color: whitesmoke;
  margin-bottom: 10px;
  padding: 10px;
  cursor: pointer;
`;

const Item = styled.div`
`;

const LibraryIcon = styled.img.attrs({ src: library })`
  width: 25px;
`;

export function Region(props) {
  const { region } = props;
  const { dispatch } = useContext(ContextApp);

  return (
      <Wrapper to={`/regions/${region.order}`} onClick={() =>
      {
        dispatch({
          type: 'getRegion',
          payload: {
            region: region
          }
        })
      }}
      >
        <Item>
          {region.fullname}
        </Item>
        <Item>
          <LibraryIcon />
          {region.libraries}
        </Item>
      </Wrapper>
  );
}
