import React, {  useContext } from 'react';
import styled from 'styled-components';

import { ContextApp } from "../../../../reducers/getRegion";

const Title = styled.h1`
  color: gray;
`;

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

const Content = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  margin-bottom: 10px;
`;

const Bold = styled.div`
  font-weight: bolder;
  margin-right: 10px;
`;

export function Join() {
  const { state: { region } } = useContext(ContextApp);

  if (!region) return <Title>Я не нашел ендпоинт для получения данных по конкретному региону</Title>;
  return (
    <Wrapper>
      <Title>{region.fullname}</Title>
      <Content>
        <Item>
          <Bold>Название:</Bold>
          {region.formname}
        </Item>

        <Item>
          <Bold>Адрес:</Bold>
          {region.address}
        </Item>

        <Item>
          <Bold>Количество библиотек:</Bold>
          {region.libraries}
        </Item>

        <Item>
          <Bold>Количество компьютеров:</Bold>
          {region.computers}
        </Item>

        <Item>
          <Bold>Количество сотрудников:</Bold>
          {region.employees}
        </Item>
      </Content>
    </Wrapper>
  );
}
