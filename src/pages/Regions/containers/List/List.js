import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import { getData } from "../../../../api";
import { Region } from "../../components/Region/Region";

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 20px;
`;

export function List() {
  const [data, setData] = useState([]);

  useEffect(() => {
    getData().then(setData);
  }, []);

  return (
    <Wrapper>
      {
        data.map((region) => <Region key={region.order} region={region} />)
      }
    </Wrapper>
  );
}
