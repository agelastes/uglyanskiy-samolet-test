import React from "react";

export const ContextApp = React.createContext();

export const initialState = {
};

export const getRegionReducer = (state, action) => {
  switch(action.type) {
    case 'getRegion': {
      return {
        ...state,
        ...action.payload
      };
    }
    default:
      return state
  }
};
